﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpordipaevKlassiga
{
    class Program
    {
        static void Main()
        {
            string filename = "..\\..\\spordipäeva protokoll.txt";
            string[] tulemusedtxt = System.IO.File.ReadAllLines(filename);
            List<Tulemus> tulemused = new List<Tulemus>();
            for (int i = 1; i < tulemusedtxt.Length; i++)
            {
                string[] reaosad = tulemusedtxt[i].Split(',');
                tulemused.Add(
                    new Tulemus
                    {
                        Nimi = reaosad[0],
                        Distants = double.TryParse(reaosad[1], out double d) ? d : 0,
                        Aeg = double.TryParse(reaosad[2], out double a) ? a : 0,
                    }
                    ) ;
            }

            foreach (var x in tulemused.OrderByDescending(x => x.Kiirus))
                Console.WriteLine($"{x.Nimi} \tjooksis {x.Distants} \tkiirusega {x.Kiirus:F2}");

            // ma tahaks leida kiireima
            int kiireim = 0;
            for (int i = 1; i < tulemused.Count; i++)
            {
                if (tulemused[i].Kiirus > tulemused[kiireim].Kiirus) kiireim = i;
            }
            Console.WriteLine($"Kiireim oli {tulemused[kiireim].Nimi}");


        }
    }

    class Tulemus
    {
        public string Nimi { get; set; }
        public double Distants { get; set; }

        public double Aeg { get; set; }

        public double Kiirus => Distants / Aeg;
    }
}
