﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// TODO: hiljem korjame siit ülearusd read ära

namespace KlassidAlgus
{
    class Program
    {
        static void Main(string[] args)
        {
            // alustaks nüüd progemisega

            //ÜtleTere();
            //ÜtleTere("Henn");
            //TervitaPikalt("Henn", "Sarv");
            //ÜtleTere(5);

            Console.WriteLine("Kes sa oled: ");
            string kes = Console.ReadLine();
            Console.WriteLine($"Tere {ToProper(kes)}!");

            Inimene mina = new Inimene();
            mina.Nimi = "Henn";
            mina.Vanus = 64;

            Inimene tema = new Inimene();
            tema.Nimi = "Raja Teele";
            Console.WriteLine(tema.Vanus);

            Inimene[] inimesed = new Inimene[10];
            inimesed[0] = mina;
            inimesed[1] = tema;

            //Console.WriteLine(mina);
            //Console.WriteLine(tema);


            mina.Trüki();
            tema.Trüki();

            Console.WriteLine(mina.OletatavSünniaasta()) ;




        }

        static void ÜtleTere()
        {
            Console.WriteLine("Tere!");
        }

        static void ÜtleTere(string nimi)
        {
            Console.WriteLine($"Tere {nimi}!");
        }

        static void ÜtleTere(int mitu)
        {
            for (int i = 0; i < mitu; i++) Console.WriteLine($"Tere {i+1}. korda ");
        }

        static void TervitaPikalt(string eesNimi, string pereNimi)
        {
            Console.WriteLine($"Tere {eesNimi}!");
            Console.WriteLine($"Mul on hea meel näha kedagi perekonnast {pereNimi}");
        }

        //static string ToProper(string tekst)
        //{
        //    // kuidas teha suure algustähega nimeks
        //    string esitäht = tekst.Substring(0, 1);
        //    string ülejäänud = tekst.Substring(1); // teisest märgist lõpuni

        //    return esitäht.ToUpper() + ülejäänud.ToLower();
        //}

        static string ToProperx(string tekst)
        {
            return tekst == "" ? "" : tekst.Substring(0, 1).ToUpper() + tekst.Substring(1).ToLower();
        }

        // vaja on etteantud string (muutuja tekst) teisendada ja anda tagasi suure algustähega
        // sõltumata sellest, milliste tähtedega ta on
        // st 1. täht on vaja teha suureks (selleks tuleks ta siis sealt võtta)
        // ja kõik ülejäänud väikseks

        // stringist tähtede või osa võtmiseks on Substring
        // teisendamiseks on toUpper ja ToLower

        /*
         * 1. "nimi" teha tükkideks (Split -> string[])
         * 2. massiiv läbi käia (tükkel? for)
         * 3. Tegema iga tükiga seda sama
         * 4. stringi uuesti kokku panema
         * */

        static string ToProper(string text)
        {
            string[] osad = text.Split(' ');
            for (int i = 0; i < osad.Length; i++)
            {
                osad[i] = ToProperx(osad[i]);
            }
            return string.Join(" ", osad);
        }


        // peale lõunat teeme see sama funktsioon ringi nii, et 
        // string tükeldadakse tühiku kohalt nimedeks
        // ja iga nimi eraldi teisendatakse suure algustähega
        // henn sarv -> Henn Sarv

        // ja siis veel nii, et sidekriipsuga nimed ka teisendatakse
        // pille-riin sarv -> Pille-Riin Sarv





    }

    
}
