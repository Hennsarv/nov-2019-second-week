﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeadProgrammis
{
    class Program
    {
        static void Main(string[] args)
        {
            // õhtu hakkab kätte jõudma - palju enam jututada täna ei saa
            // koju ma kodutöid ei anna - ebavõrdne seis, mõni saab, mõni ei saa teha neid

            // üks teema, mida läheb vaja ja mida varem oleme hiljem käsitlenud - ehk täna jõuab


            //try
            //{
                Console.Write("Anna üks arv: ");
                int yks = int.Parse(Console.ReadLine());
                Console.Write("Anna veel üks arv: ");
                int teine = int.Parse(Console.ReadLine());
                if (teine == 0) { throw new Exception("lõpeta see nulliga jagamine"); }
                Console.WriteLine($"{yks} jagatud {teine} on {yks / teine}");
            //}

            //catch (DivideByZeroException e)
            //{
            //    Console.WriteLine("jätame täna jagamata"); 

            //}
            //catch (FormatException e)
            //{
            //    Console.WriteLine("õpi kirjutama");
            //}


            //finally
            //{
            //    Console.WriteLine("\nmina pesen oma käed puhtaks\n");
            //}
            //Console.WriteLine("seda vea korral ei tehta enam");

            Inimene inim = new Inimene("Kalle", "Kusta", "kuningas");
            inim.UusPalk(100);
            inim.UusPalk(80);
            Console.WriteLine($"Palk on {inim.Palk}");

            inim.NewPalk = 120;
            inim.NewPalk = 110;
            Console.WriteLine($"Palk on {inim.Palk}");

            

        }
    }
    class Inimene
    {
        public string Eesnimi { get; }   // readonly property
        public string Perenimi { get; }
        public string Amet { get; }

        public double Palk { get; private set; }

        public Inimene(string eesnimi, string perenimi, string amet)
            => (Eesnimi, Perenimi, Amet) = (eesnimi, perenimi, amet);

        // controlled method for safe edit private writable property
        public void UusPalk(double uusPalk) => Palk = uusPalk > Palk ? uusPalk : Palk;

        // write only property
        public double NewPalk
        {
            set => Palk = value > Palk ? value : Palk;
        }

    }
}
