﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassJaMeetod
{
    class Program
    {
        static void Main(string[] args)
        { 
            int[] arvud =  { 7, 77, 777 };
            //arvud[0] = 7;
            //arvud[1] = 77;
            //arvud[2] = 777;
            List<int> teised = new List<int> { 8, 88, 888 };
            //teised.Add(8);
            //teised.Add(88);
            //teised.Add(888);

            Inimene mina = new Inimene();

            mina.Nimi = "Henn";
            mina.Vanus = 64;
            Console.WriteLine(mina);

            Inimene teineMina = new Inimene 
            { 
                Nimi = "Henn", 
                Vanus = 64 
            };

            Inimene.VanusePiir = 21;

            Inimene laps = new Inimene { Nimi = "Kalle", Vanus = 16 };

            Inimene.MyyViinaStatic(laps);

            mina.MyyViinaInstance();


            

        }

        

    }

    class Inimene
    {
        public string Nimi;
        public int Vanus;
        int palk = 0;

        public static int VanusePiir = 18;

        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {Vanus}";
        }

        public bool KasLaps()
        {
            return Vanus < VanusePiir;
        }

        public static void MyyViinaStatic(Inimene x)
        {
            if (x.KasLaps()) Console.WriteLine($"{x.Nimi} viina ei saa");
            else Console.WriteLine($"No osta siis viina {x.Nimi}");
        }

        public void MyyViinaInstance()
        {
            if (this.KasLaps()) Console.WriteLine($"{this.Nimi} viina ei saa");
            else Console.WriteLine($"No osta siis viina {this.Nimi}");
        }
    }
}
