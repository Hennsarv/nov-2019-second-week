﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassJaAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene("35503070211") //, "henn")
            {
                Nimi = "henn sarv",  // siin kasutatakse property set meetodit
                //IK = "35503070211"
            };

            Inimene pr = new Inimene("48501230123", "pille-riin sarv");
            new Inimene("48305281111", "mai-liis sarv");
            new Inimene("45705211111", "maris sarv");

            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);

            


        }
    }

    
}
