﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassJaAccess
{

    enum Gender { Female, Male}

    class Inimene
    {
        // static väljad ja propertid
        public static int InimesteArv { get; private set; } = 0;
        public static List<Inimene> Inimesed = new List<Inimene>();

        // private väljad
        private int _InimeseNr = ++InimesteArv;
        private string _Nimi;

        // propertyd
        public string IK { get; }
        public string Nimi
        {
            get => _Nimi;

            //set => _Nimi = Utils.ToProper(value); 
            set => _Nimi = value.ToProper();
        }

        // konstruktorid

        public Inimene(string IK)
        { 
            this.IK = IK;
            Inimesed.Add(this);
        }

        // konstruktoreid saab üksteise sappa kirjutada
        // sel juhul täidetakse siin "selle Teise" ja seejärel "selle" konstruktori laused
        public Inimene(string IK, string nimi) : this(IK) => _Nimi = nimi.ToProper();
        

        // arvutatavad propertid
        public DateTime DateOfBirth => 
            new DateTime(
                    (((IK[0] - '1') / 2) + 18) * 100 + // sajand
                    int.Parse(IK.Substring(1, 2)),      // aasta
                    int.Parse(IK.Substring(3, 2)),      // kuu
                    int.Parse(IK.Substring(5, 2))       // päev
            );
        #region get variandid
        //{
        //get
        //{
        //    //// variant 1
        //    //int sajand = 19;
        //    //if (IK.Substring(0, 1) == "1" || IK.Substring(0, 1) == "2") sajand = 18;
        //    //else if (IK.Substring(0, 1) == "5" || IK.Substring(0, 1) == "6") sajand = 20;

        //    //// variant 2 - switch
        //    //switch(IK.Substring(0,1))
        //    //{
        //    //    case "1": case "2": sajand = 18; break;
        //    //    case "3": case "4": sajand = 19; break;
        //    //    case "5": case "6": sajand = 20; break;
        //    //}

        //    // variant 3
        //    // sajand = ((_IK[0] - '1') / 2) + 18;

        //    return new DateTime(
        //        //sajand veel puudu
        //        (((_IK[0] - '1') / 2) + 18) * 100 +
        //        int.Parse(IK.Substring(1, 2)),
        //        int.Parse(IK.Substring(3, 2)),
        //        int.Parse(IK.Substring(5, 2))

        //        );
        //} 
        //}
        #endregion


        //public string GetIk() => _IK;
        //public string GetNimi() => _Nimi;
        //public void SetNimi(string uusnimi) => _Nimi = ToProper(uusnimi);

        public int Age => (DateTime.Today - DateOfBirth).Days * 4 / 1461;

        public Gender Gender => (Gender)(IK[0] % 2);


        // overrided
        public override string ToString() => $"{_InimeseNr}. Inimene {_Nimi} (IK={IK})";

        // muud asjad
       

    }

    static class Utils // staatiline klass, mis sisaldab vaid staatilisi asju
    {
        // näide extension funktsioonist, mis "laiendab" string klassi ja lubab seda funktsiooni käivitada kahe moodi
        // Utils.ToProper(parameeter-string)
        // parameeter-string.ToProper()
        public static string ToProperX(this string tekst) 
            => tekst == "" ? "" : tekst.Substring(0, 1).ToUpper() + tekst.Substring(1).ToLower();

        // tegin natuke ToProper() funktsiooni ringi
        // esialgse variandi nimetasin teise nimega (siis mul lihtsam)
        // tegin uue funktsiooni, mis teeb mida
        // 1. splitib teksti (enamasti nimi) tühikute kohalt - saab massiivi
        // 2. muutab selle massiivi elemendid (eelmist funktsiooni kasutades) suure algustähega
        //      NB! ära praegu küsi, mis on select - me saame seda veel tunda, aga see on mugav asi
        // 3. paneb selle saaadud tulemuse string.join abil uuesti kokku
        //
        // Lisaks (selle idee andis üks õpilane - sellega saab sidekriipsuga nimed ka "õigeks"
        // ENNE splittimist asendab kõik sidekriipsud "-" kombinatsiooniga sidekriip-tühik "- "
        // PEALE tagasi kokkupanemist asendab need "- " uuesti lihtsalt "-" (korjab ülearused tühikud vahel välja)


        public static string ToProper(this string tekst)
            => string.Join(" ", tekst.Replace("-", "- ").Split(' ').Select(x => x.ToProperX())).Replace("- ", "-");
            //=> string.Join(" ", tekst.Split(' ').Select(x => x.ToProperX()));

        // kes ikka ei kannata ja tahavad teada, mis see select on, siis see on selline asi, mis võtab massiivi (kollektsiooni)
        // järjest kõik elemendid (x) ja teeb neist uued (x.ToProperX()) ja saab niimoodi uue massiivi (kollektsiooni tegelikult)
    }
}
