﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlasssideTuletamiseÜlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Kujund> kujundid = new List<Kujund>
            {
                new Ring(10),
                new Ruut(10),
                new Kolmnurk(7,4),
                new Ristkülik(10,4)
            };
            foreach (var x in kujundid) Console.WriteLine($"{x} pindala: {x.Pindala:F2}");

            Console.WriteLine(kujundid.Sum(x => x.Pindala));

            List<Inimene> koolipere = new List<Inimene>
            {
                new Õpilane {Nimi = "Mati Maasikas", Klass= "1B"},
                new Õpilane {Nimi = "Kati Kaalikas", Klass= "1B"},
                new Õpilane {Nimi = "Pille Palukas", Klass= "2C"},
                new Õpilane {Nimi = "Malle Mustikas", Klass= "2C"},

                new Õpetaja {Nimi = "Henn Sarv", Aine = "matemaatika"},
                new Õpetaja {Nimi = "Kalle Kusta", Aine = "ajalugu"},
                new Õpetaja {Nimi = "Malle Mallikas", Aine = "eesti keel"},

                new Inimene {Nimi = "direktor Kalju"}

            };
            foreach (var x in koolipere) Console.WriteLine(x);

            foreach( var x in koolipere)
            {
                switch(x) // seda switchi nimetatakse pattern-match switciks
                {
                    case Õpilane pumbu:
                        Console.WriteLine($"{pumbu.Nimi} läheb kolhoosi porgandeid korjama nii nagu kogu {jumbu.Klass}");
                        break;

                    case Õpetaja õps:
                        Console.WriteLine($"{õps.Nimi} saab vaba päeva, et õppida {õps.Aine}");
                        break;

                    default:
                        Console.WriteLine($"{x.Nimi} jääb kooli valvama");
                        break;
                }

                // siin see switch ja if teevad sama asja
                if (x is Õpilane jumbu) { Console.WriteLine($"{jumbu.Nimi} läheb kolhoosi porgandeid korjama nii nagu kogu {jumbu.Klass}"); }
                else if (x is Õpetaja õps) { Console.WriteLine($"{õps.Nimi} saab vaba päeva, et õppida {õps.Aine}"); }
                else { Console.WriteLine($"{x.Nimi} jääb kooli valvama"); }

            }

        }
    }

    abstract class Kujund
    {
        public abstract double Pindala { get; }
    }

    class Ruut : Kujund
    {
        public double Serv { get; }

        public override double Pindala => Serv * Serv;

        public Ruut(double serv) => Serv = serv;

        public override string ToString() => $"Ruut {Serv}x{Serv}";
    }

    class Ristkülik : Kujund
    {
        public double Kõrgus { get; }
        public double Laius { get; }

        public override double Pindala => Kõrgus * Laius;

        public Ristkülik(double kõrgus, double laius) => (Kõrgus, Laius) = (kõrgus, laius);
        public override string ToString() => $"Ristkülik {Laius}x{Kõrgus}";

    }
    class Kolmnurk : Kujund
    {
        public double Kõrgus { get; }
        public double Alus { get; }

        public override double Pindala => Kõrgus * Alus / 2;

        public Kolmnurk(double kõrgus, double alus) => (Kõrgus, Alus) = (kõrgus, alus);
        public override string ToString() => $"Kolmnurk alus:{Alus} kõrgus:{Kõrgus}";

    }

    class Ring : Kujund
    {
        public double Raadius { get; }

        public override double Pindala => Raadius * Raadius * Math.PI;

        public Ring(double raadius) => Raadius = raadius;

        public override string ToString() => $"Ring R:{Raadius}";

    }

    class Inimene
    {
        public string Nimi;
        public override string ToString() => $"{Nimi}";
    }

    class Õpilane : Inimene
    {
        public string Klass;
        public override string ToString() => $"{Klass} klassi õpilane {Nimi}";
    }

    class Õpetaja : Inimene
    {
        public string Aine;
        public override string ToString() => $"{Aine} õpetaja {Nimi}";

    }


}
