﻿using System;

namespace ConsoleCore
{
    enum Gender { Female, Male}
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] arvud = { 1,2,3,4,5,6,7};
            var arvud2 = arvud[^5..];

            Console.WriteLine($"[{string.Join(",",arvud2)}]");
            Console.WriteLine("Hello World!"[6..8]);

            var x = 7..;
            Console.WriteLine(x);

            string s = "Hello World!";
            for (int i = 4; i < s.Length; i++) 
            {
                Console.WriteLine(s[(i)..]);
            }

            Gender sugu = Gender.Female;

            string sg = sugu switch
            {
                Gender.Female => "Naine",
                Gender.Male => "Mees",
                _ => "tundmatu"

            };





        }
    }
}
