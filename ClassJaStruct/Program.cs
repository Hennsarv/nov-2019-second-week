﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassJaStruct
{
    class Program
    {
        static void Main(string[] args)
        {
            // kui inimene on Class, siis mõlemad muutujad sisaldavad SAMA eksemplari
            InimeneClass esimeneC = new InimeneClass();
            esimeneC.Nimi = "Henn";
            esimeneC.Vanus = 64;
            InimeneClass teineC = esimeneC;
            teineC.Nimi = "Sarviktaat";
            Console.WriteLine(esimeneC);
            Console.WriteLine(teineC);

            // kui inimene on struct, siis igas muutujas on OMA eksemplar
            InimeneStruct esimeneS; // = new InimeneStruct();
            esimeneS.Nimi = "Henn";
            esimeneS.Vanus = 64;
            InimeneStruct teineS = esimeneS;
            teineS.Nimi = "Sarviktaat";
            Console.WriteLine(esimeneS);
            Console.WriteLine(teineS);



            //int[] arvud = { 1, 2, 3, 4, 5 };
            //int[] teised = (int[])arvud.Clone();
            //teised[2] = 7;
            //Console.WriteLine($"{{{string.Join(",", arvud)}}}");


        }
    }

    // ma teen HÄSTI lihtsa classi ja structi, siis saame proovida üht asja

    class InimeneClass
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene (class) {Nimi} vanusega {Vanus}";
    }
    struct InimeneStruct
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene (struct) {Nimi} vanusega {Vanus}";
    }
}
