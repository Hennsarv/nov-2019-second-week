﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ÕpilasedHinded
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "..\\..\\Õpilased.txt";

            // hiljem on vaja meil hindeid ja õpilasi kokku panna (isikukoodi järgi)
            // seepärast oleks hea kui need õpilased läheks Dictionary

            var Õpilased = System.IO.File.ReadAllLines(filename)
                .Select(x => x.Split(','))
                .Select(x => new { IK = x[0].Trim(), Nimi = x[1].Trim(), Klass = x[2].Trim() })
                .ToDictionary(x => x.IK);

            // kuna me jub aoleme korra õpilased sisse lugenud ja dictionary pannud
            // meil ju ei ole vaja neid enam uuesti lugeda ja splittida ja trimmida

//            var list = System.IO.File.ReadAllLines(filename)
            var list = Õpilased.Values
//                .Select(x => x.Split(','))
//                .Select(x => new { IK = x[0].Trim(), Nimi = x[1].Trim(), Klass = x[2].Trim()})
                .GroupBy(x => x.Klass)
                ;
            foreach (var v in list)
            {
                Console.WriteLine($"Klassi {v.Key} nimekiri");
                foreach(var n in v)
                    Console.WriteLine($"\t{n.Nimi} - {n.IK}");
            }

            var Hinded = System.IO.File.ReadAllLines("..\\..\\hinded.txt")
                .Select(x => x.Split(','))
                .Select(x => new { ÕpilaseIK = x[1].Trim(), ÕpetajaIK = x[0].Trim(), Aine = x[2].Trim(), Hinne = int.Parse(x[3])})
                ;
            foreach (var h in Hinded) 
                Console.WriteLine(
                    "Õpilane {0} sai aines {1} hindeks {2}"
                    , (Õpilased.ContainsKey(h.ÕpilaseIK) ? Õpilased[h.ÕpilaseIK].Nimi : h.ÕpilaseIK)
                    , h.Aine
                    , h.Hinne
                    );

            var ÕpilasteKeskmised = Hinded
                .GroupBy(x => x.ÕpilaseIK)
                .Select(x => new { Nimi = Õpilased[x.Key].Nimi, Keskmine = x.Average(y => y.Hinne) });

            foreach (var v in ÕpilasteKeskmised) Console.WriteLine(v);

            // leia üles iga aine parim õpilane!

            var AineParim = Hinded
                .GroupBy(x => x.Aine)
                .Select(x => new { Aine = x.Key, Parim = x.OrderByDescending(y => y.Hinne).First()  })
                .Select(x => new { x.Aine, Parim = Õpilased[x.Parim.ÕpilaseIK].Nimi, Hinne = x.Parim.Hinne });

            foreach (var v in AineParim) Console.WriteLine(v);

            var AineParimad = Hinded
                .GroupBy(x => new { x.Aine, Klass = Õpilased[x.ÕpilaseIK].Klass })
                .Select(x => new { Aine = x.Key, Parimad = x, ParimHinne = x.Max(y => y.Hinne) })
                .Select(x => new { x.Aine, Parimad = x.Parimad.Where(y => y.Hinne == x.ParimHinne) })
                ;

            foreach(var a in AineParimad)
            {
                Console.WriteLine("parimad õpilased aines: " + a.Aine);
                foreach(var v in a.Parimad)
                    Console.WriteLine($"\t{Õpilased[v.ÕpilaseIK].Nimi} - hindega {v.Hinne}");
            }

            var aineteKlassideKaupa = Hinded
                .GroupBy(x => x.Aine)
                .Select(x => new { Aine = x.Key, HinnneteKaupa = x.GroupBy(y => y.Hinne) })
                ;

            foreach(var a in aineteKlassideKaupa.OrderBy(x => x.Aine))
            {
                Console.WriteLine($"aines {a.Aine} said õpilased selliseid hindeid:");
                foreach(var v in a.HinnneteKaupa.OrderByDescending(x => x.Key))
                {
                    Console.WriteLine($"\tHinde {v.Key} said:");
                    foreach(var x in v)
                        Console.WriteLine( "\t\t"+ Õpilased[ x.ÕpilaseIK ].Nimi);
                }
            }
           
        }
    }
}
