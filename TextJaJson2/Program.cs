﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;


namespace TextJaJson2
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "..\\..\\spordipäeva protokoll.txt";
            string filenameJson = "..\\..\\protokoll.json";

            if (false)
            {
                var protok = File.ReadAllLines(filename)
                              .Skip(1)
                              .Select(x => x.Split(','))
                              .Where(x => x.Length > 2)
                              .Select(x => new Tulemus { Nimi = x[0], Distants = Convert.ToInt32(x[1]), Aeg = int.Parse(x[2]), /*Kiirus = double.Parse(x[1]) / int.Parse(x[2])*/ })
                              //.Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2])})
                              //.Select(x => new Tulemus { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg})
                              .ToList()
                              ;

                string json = JsonConvert.SerializeObject(protok);
                // Console.WriteLine(json);
                File.WriteAllText(filenameJson, json);

            }

            string loeJson = File.ReadAllText(filenameJson);
            var tulemused = JsonConvert.DeserializeObject<List<Tulemus>>(loeJson);
            foreach (var x in tulemused) Console.WriteLine($"{x.Nimi} jooksis {x.Distants} ajaga {x.Aeg}");

            var õpsid = File.ReadAllLines("..\\..\\Õpetajad.txt")
                .Select( x => (x+",,,").Split(','))
                .Select(x => new  { IK = x[0].Trim(), Nimi = x[1].Trim(), Aine = x[2].Trim(), 
                    KlassMidaJuhatab = x[3].Trim()
                })
                ;
            foreach (var x in õpsid) Console.WriteLine(x);

            int[,] tabel = { { 1, 2, 3 }, { 4, 5, 6 } };
            string uus = JsonConvert.SerializeObject(tabel);
            Console.WriteLine(uus);
            int[,] teeme = JsonConvert.DeserializeObject<int[,]>(uus);

        }
    }

    public class Õpetaja
    {
        public string IK { get; set; }
        public string Nimi { get; set; }
        public string Aine { get; set; }

        public string KlassMidaJuhatab { get; set; }

    }


    public class Tulemus
    {
        public string Nimi { get; set; }
        public int Distants { get; set; }
        public int Aeg { get; set; }
        public double Kiirus {
            get => (double) Distants / Aeg;
            set {}
        } 

    }
}
