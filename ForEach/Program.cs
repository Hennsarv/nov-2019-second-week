﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForEach
{
    static class E
    {
        // siia ma hakkan oma funktsioone ehitama
        public static IEnumerable<int> Paaris(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
                if (x % 2 == 0) yield return x;
        }
        public static IEnumerable<int> Paaritu(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
                if (x % 2 != 0) yield return x;
        }
        public static IEnumerable<int> Esimesed(this IEnumerable<int> mass, int mitu)
        {
            int mitmes = 0;
            foreach (var x in mass)
                if (mitmes++ < mitu) yield return x; else break;
        }

        public static IEnumerable<int> Millised(this IEnumerable<int> mass, Func<int, bool> f)
        {
            foreach (var x in mass) if (f(x)) yield return x;
        }

        //public static int Ruut(int x) { return x * x; }

    }

    class Program
    {
        static void Main(string[] args)
        {
            //Func<int, int> ruut = x => x * x;
            //Console.WriteLine( ruut(4));
            //Func<int, bool> kasSuur =  x => x > 5;
            //if (((Func<int,bool>)(x => x > 5))(ruut(4))) Console.WriteLine("on küll suur");


            // nüüd me õpime midagi ILUSAT!
            int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            foreach (var x in arvud
                //.Esimesed(5)
                //.Paaritu()
                .Where(x => x % 2 == 0)
                //                .Where(x => x < 10)
                //.Skip(3)
                //.Take(3)

                ) Console.WriteLine(x);

            Console.WriteLine(arvud.Where(x => x > 5).First());

            foreach (var x in arvud
                .Select(x => new { arv = x, ruut = x * x })
                .Where(x => x.ruut > 100)
                ) Console.WriteLine(x);

            var protok = System.IO.File.ReadAllLines("..\\..\\spordipäeva protokoll.txt")
                .Skip(1)
                .Select(x => x.Split(','))
                .Where(x => x.Length > 2)
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]), Kiirus = double.Parse(x[1]) / int.Parse(x[2]) })
                //.Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2])})
                //.Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg})
                .ToList()
                ;

            foreach (var x in protok) Console.WriteLine(x);

            //Console.WriteLine("kiireim oli: " +
            //protok.OrderByDescending(x => x.Kiirus).First().Nimi
            //);

            //var q = (from x in protok
            //        where x.Distants == 100
            //        orderby x.Kiirus descending
            //        select x.Nimi).FirstOrDefault();
            //Console.WriteLine(q);

            //var q1 = protok.GroupBy(x => x.Distants)
            //    .Select(x => x.OrderByDescending(y => y.Kiirus).FirstOrDefault())
            //    .ToList();

            //foreach (var x in q1) Console.WriteLine(x);
            //string filename = @"C:\Users\sarvi\Source\Repos\TeineNädal\ForEach\spordipäeva protokoll.txt";
            //filename = filename.Split('\\').Last();
            //int[] katse = { };

            //var q3 =arvud.GroupBy(x => x % 2).Select(x => x.Average());

            // Proovi - kas sa suudad torutehetega leida kõige stabiilsema jooksja
            // * jooksnud vähemlat 2 distnatsi
            //  * kiiruste erinevus minimaalne

            Console.WriteLine(
            protok                      // protokoll
                .ToLookup(x => x.Nimi)   // portsionid nimede kaupa (koos portsioni nimega
                .Where(x => x.Count() > 1)          // vähemalt kaks distantsi
                //.Select(x => new { Nimi = x.Key, Min = x.Min(y => y.Kiirus), Max = x.Max(y => y.Kiirus) })
                .Select(x => new {Nimi = x.Key, Vahe = x.Max(y => y.Kiirus) - x.Min(y => y.Kiirus)})
                
                // nimi, maksimaalne ja minimaalne kiirus
                //.Select(x => new { x.Nimi, x.Min, x.Max, Vahe = x.Max - x.Min })   // nimi , vahe
                .OrderBy(x => x.Vahe)
                .Take(1)
                .Select(x => $"kõige stabiilsem oli {x.Nimi} kelle kiiruste vahe oli {x.Vahe}")
                .SingleOrDefault()
                );

            var averaag = Enumerable.Range(1, 10)
                .Select(x => (int?)x)
                .Where(x => x > 50)
                .DefaultIfEmpty(null)
                .Average();             // AverageOrDefault()
                ;
            Console.WriteLine(averaag?.ToString()??"keskmist pole");



        }

    }
}
